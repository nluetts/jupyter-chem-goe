#Oberbibliotheken zum Plotten.
import matplotlib.pyplot as plt

#Ermöglicht numpy-Arrays, effiziente Berechnung von Mittelwerten, Standardabweichung,...
import numpy as np
#Anzahl an Nachkommastellen, nach eigenem belieben Änderbar
np.set_printoptions(precision=3,suppress=False)

#Ermöglicht Least-Square mit BELIEBIGEN Funktionen + gibt Kovarianzmatrix aus
from scipy.optimize import curve_fit
#Effizienteres Modul für reine lineare Regression
#Wenn nichts für curve_fit wie die Restriktion bzgl. Variablen spricht oder Fits höherer Ordnung, ist linregress zu bevorzugen
#############################################################
#Ab Scipy 1.6 wird auch intercept_stderr zur Verfügung stehen 
#############################################################
#import scipy
#print(scipy.__version__)
from scipy.stats import linregress

#Numerische Integration
#Wahl ist den Studierenden überlassen
from scipy.integrate import simps
from scipy.integrate import trapz
from scipy.integrate import cumtrapz
from scipy.integrate import romb

#Bibliothek für Algebra. 
#Es werden hier nur die benötigten Befehle importiert
#Diese hängen von Versuch zu Versuch ab, das allgemeine Skript importiert sehr vieles
from sympy import symbols, lambdify, log, sqrt,diff,exp, solve
#Nicht notwendig, kann aber für schönere Printing-Befehle genutzt werden
from sympy import pprint,factor,expand

