# Aufbau der Dateien
Die Notebooks stellen den jeweiligen __Zwischenstand__  dar und sind daher nicht unbedingt fertig. Mit einem gegebenen Datensatz wird die vollständige Auswertung durchgeführt. _Die Studierenden erhalten später eine modifizierte Fassung, die sie selber ausfüllen müssen_.
